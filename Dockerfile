FROM python:3.10.12

RUN mkdir /todo_app

COPY requirements.txt /todo_app/

RUN python -m pip install -r /todo_app/requirements.txt

COPY ./ /todo_app

WORKDIR /todo_app

EXPOSE 8000

CMD python manage.py runserver 0.0.0.0:8000