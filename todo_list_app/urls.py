from django.urls import path
from . import views
 
urlpatterns = [

      path( "",
            views.TodoListView.as_view(),
            name="todo-list" ),
            
      path( "todo-list/<int:pk>",
            views.TodoListDetailView.as_view(),
            name="todo-list-detail" ),

      path( "create",
            views.TodoListCreateView.as_view(),
            name="todo-list-create" ),

      path( "todo-list/<int:pk>/update",
            views.TodoListUpdateView.as_view(),
            name="todo-list-update" ),

      path( "todo-list/<int:pk>/delete",
            views.TodoListDeleteView.as_view(),
            name="todo-list-delete" ),      
]