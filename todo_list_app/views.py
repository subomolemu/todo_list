from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import TodoList


class TodoListView(ListView):
    model = TodoList
    queryset = TodoList.objects.all().order_by('-date_created')


class TodoListDetailView(DetailView):
    model = TodoList  


class TodoListCreateView(CreateView):
    model = TodoList
    fields = ['title','descriptions']
    success_url = reverse_lazy('todo-list')    

    

class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ['title','descriptions']

    def get_success_url(self):
        return reverse_lazy('todo-detail', 
                            kwargs={'pk': self.todolist.id})
    

class TodoListDeleteView(DeleteView):
    model = TodoList
    success_url = reverse_lazy("todo-list")