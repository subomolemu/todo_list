from django.test import TestCase
from django.urls import reverse
from todo_list_app.models import TodoList
from django.utils import timezone

class TodoListTestCase(TestCase):   

    @classmethod    
    def setUpTestData(cls):
        cls.max_title_length = 200
        cls.title_data = 'Заголовок 1'
        cls.desc_data = 'Описание 1'
        TodoList.objects.create(title=cls.title_data, descriptions=cls.desc_data)

    def setUp(self):        
        self.title = TodoList.objects.get(id=1)
        self.date_created = self.title.date_created

    def test_title_label(self):        
        title_lable = self.title._meta.get_field('title').verbose_name
        self.assertEquals(title_lable, 'title')

    def test_desc_label(self):        
        title_lable = self.title._meta.get_field('descriptions').verbose_name
        self.assertEquals(title_lable, 'descriptions')
    
    def test_date_created_label(self):        
        title_lable = self.title._meta.get_field('date_created').verbose_name
        self.assertEquals(title_lable, 'date created')
        
    def test_title_max_length(self):
        max_length = self.title._meta.get_field('title').max_length
        self.assertEquals(max_length, self.max_title_length)

    def test_title_data(self):
        title_test_data = self.title.title
        self.assertEquals(title_test_data, self.title_data)
    
    def test_description_data(self):
        desc_test_data = self.title.descriptions
        self.assertEquals(desc_test_data, self.desc_data)

    def test_description_data(self):
        date_test_data = self.title.date_created
        self.assertEquals(str(date_test_data), str(self.date_created))

    def test_detail_url(self):
        resp = self.client.get(f'/todo-list/{self.title.id}')
        self.assertEquals(resp.status_code, 200)

    def test_main_url(self):
        resp = self.client.get('')
        self.assertEquals(resp.status_code, 200)

    def test_create_url(self):
        resp = self.client.get('/create')
        self.assertEquals(resp.status_code, 200)

    def test_update_url(self):
        resp = self.client.get(f'/todo-list/{self.title.id}/update')
        self.assertEquals(resp.status_code, 200)

    def test_delete_url(self):
        resp = self.client.get(f'/todo-list/{self.title.id}/delete')
        self.assertEquals(resp.status_code, 200)